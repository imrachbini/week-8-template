from os import environ


APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', '372460726:AAEtZQGWkIYo6DCN16sh3JzANsveliCWtDM')
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://zodiakanjay.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://zodiakanjay.herokuapp.com/')
